using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    
    void OnGUI()
    {
        //Output the rotation rate, attitude and the enabled state of the gyroscope as a Label
        GUI.Label(new Rect(500, 300, 200, 40), "Battery Level: " + UnityEngine.SystemInfo.batteryLevel);
        GUI.Label(new Rect(500, 350, 200, 40), "Battery Status: " + UnityEngine.SystemInfo.batteryStatus);
        // if (Utils.ShowGUIButton("返回"))
        // {
        //     GameObject obj = Resources.Load<GameObject>("Prefabs/Main");
        //     Instantiate(obj);
        //     DestroyImmediate(gameObject);
        // }
        Utils.ReturnMain(gameObject);
        
    }
    
  
}
