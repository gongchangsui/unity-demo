using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckCollider : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform cubeTrans;
    public Transform sphereTrans;
    public Rigidbody _rigidbody;
    void Start()
    {
        
    }

    // Update is called once per frame

    private int count = 0;
    int ori = 1;

    void Update()
    {
        // transform.Translate(Vector2.right * 1 * Time.deltaTime);
        // sphereTrans.Translate(Vector2.right * 1 * Time.deltaTime);
        // cubeTrans.Translate(Vector2.left * 1 * Time.deltaTime);
        Vector3 pos = sphereTrans.position;
        Physics.Raycast(pos, Vector3.right, out hit);
        hits = Physics.SphereCastAll(pos, 100f, Vector3.right);
        // Debug.Log(hits.Length);
        // Debug.Log("ZDQ");
        // foreach (var a in hits)
        // {
        //     Debug.Log(a.point);
        //     Debug.Log("zdq in foreach");
        // }


        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        // count++;
        // if (count % 2500 == 0)
        // {
        //     ori *=-1;
        // }
        Vector3 m_Input = new Vector3(-1* ori, 0, 0) * Time.deltaTime * 5;
        _rigidbody.MovePosition(cubeTrans.position + m_Input);
    }

    private RaycastHit hit;
    private RaycastHit[] hits;
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (hits == null)
            return;
        foreach(var h in hits) 
            Gizmos.DrawLine(sphereTrans.position, h.point); 
    }
    void OnTriggerEnter(Collider collider)
    {
        //进入触发器执行的代码
        Debug.Log("ZDQ Trigger Enter");
    }
    void OnCollisionEnter(Collision collision) 
    {
        Debug.Log("ZDQ Collision Enter");
    }

    void OnCollisionStay(Collision other)
    {
        Debug.Log("ZDQ Collision Stay");
    }

    void OnCollisionExit(Collision other)
    {
        Debug.Log("ZDQ Collision exit");
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("ZDQ OnCollisionEnter2D ");
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        Debug.Log("ZDQ OnCollisionStay2D ");
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        Debug.Log("ZDQ OnCollisionExit2D ");
    }
    
    void OnGUI()
    {
        //Output the rotation rate, attitude and the enabled state of the gyroscope as a Label
        GUI.Label(new Rect(500, 300, 200, 40), "Battery Level: " + UnityEngine.SystemInfo.batteryLevel);
        GUI.Label(new Rect(500, 350, 200, 40), "Battery Status: " + UnityEngine.SystemInfo.batteryStatus);
        // if (Utils.ShowGUIButton("返回"))
        // {
        //     GameObject obj = Resources.Load<GameObject>("Prefabs/Main");
        //     Instantiate(obj);
        //     DestroyImmediate(gameObject);
        // }
        Utils.ReturnMain(gameObject);
        
    }

}
