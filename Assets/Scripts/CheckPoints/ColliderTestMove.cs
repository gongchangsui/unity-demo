using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderTestMove : MonoBehaviour
{

    private CharacterController cc;
    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {   
        Vector3 m_Input = new Vector3(-1, 0, 0) * Time.deltaTime * 5;
        cc.Move(m_Input);
    }
    void OnTriggerEnter(Collider collider)
    {
        //进入触发器执行的代码
        Debug.Log("ZDQ Trigger Enter");
    }
    
    void OnTriggerStay(Collider other)
    {
        Debug.Log("ZDQ Collision Stay");
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log("ZDQ Collision exit");
    }
    void OnCollisionEnter(Collision collision) 
    {
        Debug.Log("ZDQ Collision Enter");
    }

    void OnCollisionStay(Collision other)
    {
        Debug.Log("ZDQ Collision Stay");
    }

    void OnCollisionExit(Collision other)
    {
        Debug.Log("ZDQ Collision exit");
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("ZDQ OnCollisionEnter2D ");
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        Debug.Log("ZDQ OnCollisionStay2D ");
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        Debug.Log("ZDQ OnCollisionExit2D ");
    }

    /// <summary>
    /// 用于检测其他碰撞体是否碰撞了此方法。
    /// </summary>
    /// <param name="hit"></param>
    void OnControllerColliderHit(ControllerColliderHit hit){

        //方法体
        Debug.Log("ZDQ in OnControllerColliderHit");

    } 
    //作者：LuJack https://www.bilibili.com/read/cv27808095/?jump_opus=1 出处：bilibili
}
