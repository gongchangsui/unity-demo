using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderTriggerMove : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void OnTriggerEnter(Collider collider)
    {
        //进入触发器执行的代码
        Debug.Log("ZDQ OnTriggerEnter");
    }
    
    void OnTriggerStay(Collider other)
    {
        Debug.Log("ZDQ OnTriggerStay");
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log("ZDQ OnTriggerExit");
    }
    void OnCollisionEnter(Collision collision) 
    {
        Debug.Log("ZDQ OnCollisionEnter");
    }

    void OnCollisionStay(Collision other)
    {
        Debug.Log("ZDQ OnCollisionStay");
    }

    void OnCollisionExit(Collision other)
    {
        Debug.Log("ZDQ OnCollisionExit");
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("ZDQ OnCollisionEnter2D ");
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        Debug.Log("ZDQ OnCollisionStay2D ");
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        Debug.Log("ZDQ OnCollisionExit2D ");
    }

}
