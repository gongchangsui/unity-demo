using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Windows;

public class ColliderTriggerTest : MonoBehaviour
{

    public Rigidbody _rig;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 m_Input = new Vector3(-1, 0, 0) * Time.deltaTime * 5;
        _rig.MovePosition(_rig.position + m_Input);
    }
    
    void OnGUI()
    {
        //Output the rotation rate, attitude and the enabled state of the gyroscope as a Label
        // GUI.Label(new Rect(500, 300, 200, 40), "Battery Level: " + UnityEngine.SystemInfo.batteryLevel);
        // GUI.Label(new Rect(500, 350, 200, 40), "Battery Status: " + UnityEngine.SystemInfo.batteryStatus);
        // if (Utils.ShowGUIButton("返回"))
        // {
        //     GameObject obj = Resources.Load<GameObject>("Prefabs/Main");
        //     Instantiate(obj);
        //     DestroyImmediate(gameObject);
        // }
        Utils.ReturnMain(gameObject);
        
    }
    
    void OnTriggerEnter(Collider collider)
    {
        //进入触发器执行的代码
        Debug.Log("ZDQ Trigger Enter");
        GUI.Label(new Rect(500, 300, 200, 40), "ZDQ Trigger Enter ");
    }
    
    void OnTriggerStay(Collider other)
    {
        Debug.Log("ZDQ Collision Stay");
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log("ZDQ Collision exit");
    }
    void OnCollisionEnter(Collision collision) 
    {
        Debug.Log("ZDQ Collision Enter");
    }

    void OnCollisionStay(Collision other)
    {
        Debug.Log("ZDQ Collision Stay");
    }

    void OnCollisionExit(Collision other)
    {
        Debug.Log("ZDQ Collision exit");
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("ZDQ OnCollisionEnter2D ");
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        Debug.Log("ZDQ OnCollisionStay2D ");
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        Debug.Log("ZDQ OnCollisionExit2D ");
    }

    /// <summary>
    /// 用于检测其他碰撞体是否碰撞了此方法。
    /// </summary>
    /// <param name="hit"></param>
    void OnControllerColliderHit(ControllerColliderHit hit){

        //方法体
        Debug.Log("ZDQ in OnControllerColliderHit");

    } 
    
    #if UNITY_EDITOR
    [MenuItem("Tools/Build AssetBundle")]
        static void CreateAssetBundle()
        {
            string path = "Assets/StreamingAssets";
    
            if(Directory.Exists(path) == false)//判断这个文件夹存不存在
            {
                Directory.CreateDirectory(path);
            }
    
            BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.None, BuildTarget.OpenHarmony);
            Debug.Log("ZDQ over create Asset bundle");
        }
    #endif
}
