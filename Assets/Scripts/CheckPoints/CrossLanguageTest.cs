using System;
using UnityEngine;

namespace CheckPoints
{
    public class CrossLanguageTest: MonoBehaviour
    {
        string infoLog = "";

        private void OnGUI()
        {
            GUILayout.BeginVertical();
            Utils.ReturnMain(gameObject);
        
            if (ShowGUIButton("Add"))
            {
                ShowInfoLog("Add in Ts");
                ShowInfoLog("The Result is : " + Add());

                
            }
            GUILayout.EndVertical();
            GUILayout.Label(infoLog);


        }

        private static int Add()
        {
            var result = -1;
            #if UNITY_OPENHARMONY
                Debug.Log("ZDQ in UNITY_OPENHARMONY");
            #endif
            
            # if PLATFORM_OPENHARMONY
                Debug.Log("ZDQ in PLATFORM_OPENHARMONY");
            #endif
            # if PLATFORM_OPENHARMONY
                var hmClass = new OpenHarmonyJSClass("TestClass");
                var hmObject = hmClass.CallStatic<OpenHarmonyJSObject>("getInstance");
                result = hmObject.Call<int>("Add", 1, 2);
            #endif
            return result;
        }
        
        bool ShowGUIButton(string buttonName)
        {
            return GUILayout.Button(buttonName, GUILayout.Height(Screen.height / 20), GUILayout.Width(Screen.width / 5));
        }
        void ShowInfoLog(string info)
        {
            infoLog += info;
            infoLog += "\r\n";
            Debug.LogFormat(info);
        }
    }
}