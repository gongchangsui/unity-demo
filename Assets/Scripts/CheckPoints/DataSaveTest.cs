using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DataSaveTest : MonoBehaviour
{
    public TMP_Text currentBloodVolume;
    public TMP_Text currentScore;

    public Button AddBtn;
    public Button ReduceBtn;
    public Button LoadPlayerPrefsBtn;
    public Button SavePlayerPrefsBtn;
    public Button LoadBinBtn;
    public Button SaveBinBtn;


    public int BloodVolume = 100;
    public int Score = 0;
    private bool isCanReduce = true;
    //public static ScoreAndBlood _instance;


    // Start is called before the first frame update
    void Start()
    {
        AddBtn.onClick.AddListener(AddClick);
        ReduceBtn.onClick.AddListener(ReduceClick);
        LoadPlayerPrefsBtn.onClick.AddListener(LoadByPlayerPrefs);
        SavePlayerPrefsBtn.onClick.AddListener(SaveByPlayerPrefs);
        LoadBinBtn.onClick.AddListener(LoadByBin);
        SaveBinBtn.onClick.AddListener(SaveByBin);
    }

    // Update is called once per frame
    void Update()
    {
        currentBloodVolume.text = BloodVolume.ToString();
        currentScore.text = Score.ToString();
    }

    void OnGUI()
    {
        Utils.ReturnMain(gameObject);
    }
    private void AddClick()
    {
        BloodVolume++;
    }

    private void ReduceClick()
    {
        BloodVolume--;
    }

    private void SaveClick()
    {
        SaveByPlayerPrefs();//通过PlayerPrefs方式存储
        //SaveByBin();//通过二进制的方式存储
        //SaveByJson();//通过Json方式存储
        //SaveByXml();//通过XML方式存储
    }

    private void LoadClick()
    {
        LoadByPlayerPrefs();//通过PlayerPrefs方式读取
        //LoadByBin();//通过二进制的方式读取
        //LoadByJson();//通过Json方式读取
        //LoadByXml();//通过XML方式读取
    }
    
    private Date GetGameDate()
    {
        Date date = new Date();
        date.BloodVolume = BloodVolume;
        date.Score = Score;
        return date; 
    }
    //向游戏中加载Date中保存的数据的方法
    private void SetGameDate( Date date)
    {
        BloodVolume = date.BloodVolume;
        Score = date.Score;
    }

    
    //用Playerprefs方法对数据进行存储和读档
    private void SaveByPlayerPrefs()
    {
        PlayerPrefs.SetInt("bloodVolume", BloodVolume);
        PlayerPrefs.SetInt("score", Score);
        PlayerPrefs.Save();
    }
    private void LoadByPlayerPrefs()
    {
        if (PlayerPrefs.HasKey("bloodVolume") && PlayerPrefs.HasKey("score"))
        {
            BloodVolume = PlayerPrefs.GetInt("bloodVolume");
            Score = PlayerPrefs.GetInt("score");
        }
        else
        {
            Debug.Log("------未找到相应数据------");
        }
        
    }


    //用二进制方法对数据进行保存和读档
    private void SaveByBin()
    {
        try
        {
            Date date = GetGameDate();
            BinaryFormatter bf = new BinaryFormatter();//创建二进制格式化程序
            using (FileStream fs = File.Create(Application.persistentDataPath  + "/byBin.txt"))  //创建文件流
            {
                bf.Serialize(fs, date);
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
    }
    private void LoadByBin()
    {
        try
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream fs = File.Open(Application.persistentDataPath + "/byBin.txt", FileMode.Open))
            {
                Date date = (Date)bf.Deserialize(fs);
                SetGameDate(date);
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
    }


    //用Json方法对数据进行保存和读取
    // private void SaveByJson()
    // {
    //     Date date = GetGameDate();
    //     string datapath = Application.dataPath + "/SaveFiles" + "/byJson.json";
    //     string dateStr = JsonMapper.ToJson(date);  //利用JsonMapper将date转换成字符串
    //     StreamWriter sw = new StreamWriter(datapath); //创建一个写入流
    //     sw.Write(dateStr);//将dateStr写入
    //     sw.Close();//关闭流
    //
    // }
    // private void LoadByJson()
    // {
    //     string datePath = Application.dataPath + "/SaveFiles" + "/byJson.json";
    //     if (File.Exists(datePath ))  //判断这个路径里面是否为空
    //     {
    //         StreamReader sr = new StreamReader(datePath);//创建读取流;
    //         string jsonStr = sr.ReadToEnd();//使用方法ReadToEnd（）遍历的到保存的内容
    //         sr.Close();
    //         Date date = JsonMapper.ToObject<Date>(jsonStr);//使用JsonMapper将遍历得到的jsonStr转换成Date对象
    //         SetGameDate(date);
    //     }
    //     else
    //     {
    //         Debug.Log("------未找到文件------");
    //     }
    // }


    //使用XML方法对数据进行保存和读取
    private void SaveByXml()
    {
        Date date = GetGameDate();
        string datePath = Application.persistentDataPath + "/byXml.txt";
        XmlDocument xmlDoc = new XmlDocument();//创建XML文档
        XmlElement root = xmlDoc.CreateElement("saveByXml");//创建根节点，并设置根节点的名字
        root.SetAttribute("name", "savefile");//设置根节点的值
        //创建其他节点，并设置其他节点的值
        XmlElement bloodVolume = xmlDoc.CreateElement("bloodVolume");
        bloodVolume.InnerText = date.BloodVolume.ToString();
        XmlElement score = xmlDoc.CreateElement("score");
        score.InnerText = date.Score.ToString();
        //将子节点加入根节点，将根节点加入XML文档中
        root.AppendChild(bloodVolume);
        root.AppendChild(score);
        xmlDoc.AppendChild(root);
        xmlDoc.Save(datePath);
        
    }
    private void LoadByXml()
    {
        string datePath = Application.persistentDataPath  + "/byXml.txt";
        if (File.Exists(datePath))
        {
            Date date = new Date();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(datePath);  //加载这个路径下的xml文档

            XmlNodeList bloodVolume = xmlDoc.GetElementsByTagName("bloodVolume");//通过名字得到XML文件下对应的值
            date.BloodVolume = int.Parse(bloodVolume[0].InnerText);

            XmlNodeList score = xmlDoc.GetElementsByTagName("score");
            date.Score = int.Parse(score[0].InnerText);
            SetGameDate(date);
            
        }
        
    }
   
}



[System.Serializable]
public class Date
{
    public int BloodVolume;
    public int Score;
}