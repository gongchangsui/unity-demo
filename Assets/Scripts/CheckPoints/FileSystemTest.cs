using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class FileSystemTest : MonoBehaviour
{
    string infoLog = "";

    public Button btn;
    private GUIStyle style = new GUIStyle();

    
    // Start is called before the first frame update
    void Start()
    {
        style.fontSize = 30;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnGUI()
    {
       
        
        GUILayout.BeginVertical();

      
            GUILayout.Label("");
            GUILayout.Label("");
            GUILayout.Label("");
            GUILayout.Label("");
            GUILayout.Label("");
            GUILayout.Label("");
            GUILayout.Label("");
            GUILayout.Label("");

            GUILayout.Label("read only: streamingAssetsPath:", style);
            GUILayout.Label(Application.streamingAssetsPath, style);
            GUILayout.Label("read only: dataPath: ", style);
            GUILayout.Label(Application.dataPath, style);
            GUILayout.Label("read & write: persistentDataPath: ", style);
            GUILayout.Label(Application.persistentDataPath, style);

            //Frequency = GUILayout.TextField(Frequency, GUILayout.Width(Screen.width / 5), GUILayout.Height(Screen.height / 20));

            Utils.ReturnMain(gameObject);

            if (ShowGUIButton("CreateDirectory"))
            {
                ShowInfoLog("click CreateDirectory");
                CreateDirectoryClick();
            }
            if (ShowGUIButton("CreateFile"))
            {
                ShowInfoLog("click CreateFile");
                CreateFileClick();

            }
            if (ShowGUIButton("WriteData"))
            {
                ShowInfoLog("click WriteData");
                WriteDataClick();
            }
            if (ShowGUIButton("ReadData"))
            {
                ShowInfoLog("click ReadData");
                ReadFileClick();
            }
        
        GUILayout.EndVertical();
        GUILayout.Label(infoLog);

    }

    private void CreateFileClick()
    {
        FileInfo fileInfo= new FileInfo(Application.persistentDataPath + "/TextRead.txt");
        StreamWriter sw;

        if (!fileInfo.Exists)
        {
            sw = fileInfo.CreateText();
            ShowInfoLog("Create file");

        }
        else
        {
            sw = fileInfo.AppendText();
            ShowInfoLog("File exist!");

        }
        sw.WriteLine("new line");
        ShowInfoLog("write: new line");
        sw.Close();
        sw.Dispose();
    }

    private void ReadFileClick()
    {
        var path = Application.persistentDataPath + "/TextRead.txt";
        using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
        {
            byte[] bytes = new byte[fs.Length];
            fs.Read(bytes, 0, bytes.Length);
            fs.Close();
            string str= Encoding.UTF8.GetString(bytes);
            ShowInfoLog(str);
        }
    }

    private void CreateDirectoryClick()
    {
        var info = Directory.CreateDirectory(Application.persistentDataPath);
        if (info != null)
        {
            ShowInfoLog(info.Name + " : Create success");
        }
        else
        {
            ShowInfoLog("Create failed");
        }

    }

    private void WriteDataClick()
    {
        CreateFileClick();
    }
    
    bool ShowGUIButton(string buttonName)
    {
        return GUILayout.Button(buttonName, GUILayout.Height(Screen.height / 20), GUILayout.Width(Screen.width / 5));
    }
    void ShowInfoLog(string info)
    {
        infoLog += info;
        infoLog += "\r\n";
        Debug.LogFormat(info);
    }
}
