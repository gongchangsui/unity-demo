using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetWorkTest : MonoBehaviour
{
    private string requestUrlResult = "wait please.";
    private string requestUrlResponseCode = "wait please.";

    // Start is called before the first frame update
    void Start()
    {
        
        Debug.Log("ZDQ in network test start");
        Debug.Log("当前网络状态："+ Application.internetReachability);
        StartCoroutine(RequestUrl());
        StartCoroutine(RequestPicture());
        Debug.Log("ZDQ in network test end");

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator RequestUrl()
    {
        Debug.Log("Print Start RequestUrl");
        var url = "https://www.baidu.com";         
        var www = UnityWebRequest.Get(url);         
        yield return www.SendWebRequest();          
        requestUrlResponseCode = www.responseCode + "";
        if (www.result == UnityWebRequest.Result.ProtocolError || www.result == UnityWebRequest.Result.ConnectionError)         
        {            
            Debug.Log("ZDQ in RequestUrl");
            requestUrlResult = www.error;
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("ZDQ in RequestUrl");
            Debug.Log(www.downloadHandler.text);
            requestUrlResult = www.downloadHandler.text;
        }
        Debug.Log("Print End RequestUrl");

        
    }

    IEnumerator RequestPicture()
    {
        Debug.Log("Print Start RequestPicture");

        var url = "https://www.baidu.com/img/bd_logo1.png";
        var www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();
        Debug.Log("status code:" + www.responseCode);
        if (www.isHttpError || www.isNetworkError)
        {
            Debug.Log(www.error);
        } 
        else
        { 
            var tex = DownloadHandlerTexture.GetContent(www);
        }
    }
    
    IEnumerator InstantiateObject(string assetBundleName)
    {
        string uri = "file:///" + Application.dataPath + "/AssetBundles/" + assetBundleName;
        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(uri, 0);
        yield return request.SendWebRequest();
        AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);
        GameObject cube = bundle.LoadAsset<GameObject>("Cube");
        GameObject sprite = bundle.LoadAsset<GameObject>("Sprite");
        Instantiate(cube);
        Instantiate(sprite);
    }
    
    void OnGUI()
    {
        GUI.Label(new Rect(500, 300, 200, 40), " RequestUrl Result: " + requestUrlResult);
        GUI.Label(new Rect(500, 350, 200, 40), " RequestUrl responseCode: " + requestUrlResponseCode);
        
        GUI.Label(new Rect(500, 400, 200, 40),"当前网络状态："+ Application.internetReachability);

        Utils.ReturnMain(gameObject);
    }

}
