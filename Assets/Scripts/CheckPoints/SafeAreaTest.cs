using UnityEngine;

public class SafeAreaTest : MonoBehaviour
{
    void Update()
    {
    }
    
    void OnGUI()
    {
        GUI.color = Color.red;
        GUI.DrawTexture(new Rect(0, Screen.height - Screen.safeArea.height , Screen.safeArea.width, Screen.safeArea.height), Texture2D.whiteTexture);
        

        Debug.LogFormat("safex: {0}, safey: {1}, safewidth: {2}, safeHeight: {3}", Screen.safeArea.x, Screen.safeArea.y,
            Screen.safeArea.width, Screen.safeArea.height);
    }
}