using System;
using System.Globalization;
using UnityEngine;

public class SystemInfoTest : MonoBehaviour
{
    private int i = 0;
    //private Vector2 scrollPos;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnGUI()
    {
        //GUILayout.BeginScrollView(scrollPos, GUILayout.Width(Screen.width), GUILayout.Height(1000));

        PrintInfo();
        i = 0;
        //GUILayout.EndScrollView();
        Utils.ReturnMain(gameObject);


    }


    void PrintInfo()
    {
        PrintCounty();
        PrintLanguage();
        PrintSystemInfo();
    }

    void PrintCounty()
    {
        Print((string.Format("Print ReginInfo of CN")));
        RegionInfo myRI1 = new RegionInfo("CN"); 
        Print(string.Format( "   Name:                         {0}", myRI1.Name ));
        Print(string.Format( "   DisplayName:                  {0}", myRI1.DisplayName ));
        Print(string.Format( "   EnglishName:                  {0}", myRI1.EnglishName ));
        Print(string.Format( "   IsMetric:                     {0}", myRI1.IsMetric ));
        Print(string.Format( "   ThreeLetterISORegionName:     {0}", myRI1.ThreeLetterISORegionName ));
        Print(string.Format( "   ThreeLetterWindowsRegionName: {0}", myRI1.ThreeLetterWindowsRegionName ));
        Print(string.Format( "   TwoLetterISORegionName:       {0}", myRI1.TwoLetterISORegionName ));
        Print(string.Format( "   CurrencySymbol:               {0}", myRI1.CurrencySymbol ));
        Print(string.Format( "   ISOCurrencySymbol:            {0}", myRI1.ISOCurrencySymbol ));
    }

    void PrintLanguage()
    {
        //Debug.Log("Print Language");
        //Debug.LogFormat(" Language:  {0}", Application.systemLanguage.DisplayName());
    }

    void PrintSystemInfo()
    {
       //Utils.PrintProperties(new SystemInfo());
       //Utils.PrintProperties(new SystemInfo());
       //print(systemInfo.);
       Print(string.Format("SystemInfo.deviceModel is {0}", UnityEngine.SystemInfo.deviceModel));
       Print(string.Format("SystemInfo.deviceName is: {0}", UnityEngine.SystemInfo.deviceName));
       Print(string.Format("SystemInfo.deviceType is: {0}", UnityEngine.SystemInfo.deviceType));
       Print(string.Format("SystemInfo.deviceUniqueIdentifier is: {0}", UnityEngine.SystemInfo.deviceUniqueIdentifier));
       
       Print(string.Format("SystemInfo.graphicsDeviceName is: {0}", UnityEngine.SystemInfo.graphicsDeviceName));
       Print(string.Format("SystemInfo.graphicsDeviceID is: {0}", UnityEngine.SystemInfo.graphicsDeviceID));
       Print(string.Format("SystemInfo.graphicsDeviceType is: {0}", UnityEngine.SystemInfo.graphicsDeviceType));
       Print(string.Format("SystemInfo.graphicsDeviceVendor is: {0}", UnityEngine.SystemInfo.graphicsDeviceVendor));
       Print(string.Format("SystemInfo.graphicsDeviceVendorID is: {0}", UnityEngine.SystemInfo.graphicsDeviceVendorID));
       Print(string.Format("SystemInfo.graphicsDeviceVersion is: {0}", UnityEngine.SystemInfo.graphicsDeviceVersion));
       Print(string.Format("SystemInfo.graphicsMemorySize is: {0}", UnityEngine.SystemInfo.graphicsMemorySize));
       Print(string.Format("SystemInfo.graphicsShaderLevel is: {0}", UnityEngine.SystemInfo.graphicsShaderLevel));
       Print(string.Format("SystemInfo.maxTextureSize is: {0}", UnityEngine.SystemInfo.maxTextureSize));
       Print(string.Format("SystemInfo.graphicsMultiThreaded is: {0}", UnityEngine.SystemInfo.graphicsMultiThreaded));
       Print(string.Format("SystemInfo.systemMemorySize is: {0}", UnityEngine.SystemInfo.systemMemorySize));
       Print(string.Format("SystemInfo.operatingSystem is: {0}", UnityEngine.SystemInfo.operatingSystem));

       Print(string.Format("SystemInfo.processorCount is: {0}", UnityEngine.SystemInfo.processorCount));
       Print(string.Format("SystemInfo.processorType is: {0}", UnityEngine.SystemInfo.processorType));


    }

    void Print(string str)
    {
        // GUILayout.Button(buttonName, GUILayout.Height(Screen.height / 20), GUILayout.Width(Screen.width));

        GUI.Label(new Rect(100, 200 + i, 500, 40), str);
        i += 50;
    }
}
