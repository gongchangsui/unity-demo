// VideoPlayer 函数示例

using System;
using UnityEngine;
using UnityEngine.UI;


public class VideoTest : MonoBehaviour
{
    public Button ReturnBtn;
    void Start()
    {
        ReturnBtn.onClick.AddListener(ReturnBtnClick);

    }

    void ReturnBtnClick()
    {
        GameObject obj = Resources.Load<GameObject>("Prefabs/Main");
        Instantiate(obj);
        DestroyImmediate(gameObject);
    }

    private void OnGUI()
    {
        Utils.ReturnMain(gameObject);
    }

    void PlayVideo()
    {
        // 将一个 VideoPlayer 附加到主摄像机。
        // GameObject camera = GameObject.Find("Main Camera");
        GameObject camera = GameObject.Find("Canvas/Panel");


        // 将 VideoPlayer 添加到摄像机对象时，
        // 它会自动瞄准摄像机背板，无需更改 videoPlayer.targetCamera。
        var videoPlayer = camera.AddComponent<UnityEngine.Video.VideoPlayer>();

        // Play on Awake 默认为 true。将它设置为 false 以避免下面设置的 URL
        // 自动开始播放，因为我们处于 Start() 状态。
        videoPlayer.playOnAwake = false;

        // 默认情况下，添加到摄像机的 VideoPlayer 将使用远平面。
        // 让我们改为瞄准近平面。
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraNearPlane;

        // 这将使场景通过正在播放的视频可见。
        videoPlayer.targetCameraAlpha = 0.5F;

        // 设置要播放的视频。URL 支持本地绝对或相对路径。
        // 此处使用绝对路径。
        videoPlayer.url = Application.dataPath + "/xinwenliaobo-source.mp4";

        // 跳过前 100 帧。
        videoPlayer.frame = 100;

        // 完成后从头重新开始。
        videoPlayer.isLooping = true;

        // 每次到达结尾时，我们都会将播放速度减慢 10 倍。
        videoPlayer.loopPointReached += EndReached;

        // 开始播放。这意味着 VideoPlayer 可能需要做好准备工作（预留
        // 资源、预加载几帧等）。为了更好地控制此项准备工作
        // 带来的延迟，您可以使用 videoPlayer.Prepare() 及其
        // prepareCompleted 事件。
        videoPlayer.Play();
    }

    void EndReached(UnityEngine.Video.VideoPlayer vp)
    {
        vp.playbackSpeed = vp.playbackSpeed / 10.0F;
    }
}