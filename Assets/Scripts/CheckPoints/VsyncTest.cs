using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VsyncTest : MonoBehaviour
{
    string infoLog = "";
    private float m_time = 0.0f;


    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 500;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnGUI()
    {
        GUILayout.BeginVertical();
        Utils.ReturnMain(gameObject);
        
        if (ShowGUIButton("Add"))
        {
            ShowInfoLog("Add targetFrameRate");
            Application.targetFrameRate += 30;
        }
        if (ShowGUIButton("Reduce"))
        {
            ShowInfoLog("Reduce targetFrameRate");
            Application.targetFrameRate -= 30;
        }
        
        if (ShowGUIButton("NegativeOne"))
        {
            ShowInfoLog("Set targetFrameRate to -1");
            Application.targetFrameRate = -1;
        }
        
        GUILayout.EndVertical();
        GUILayout.Label(infoLog);
        
        m_time += (Time.unscaledDeltaTime - m_time) * 0.1f;
		
        //float ms = m_time * 1000.0f;
        float fps = 1.0f / m_time;
        
        string rate = "current refreshRate is " + Screen.currentResolution.refreshRateRatio;
        string targetFrameRate = "current targetFrameRate is " + Application.targetFrameRate;
        string frameRate = "current FPS is " + fps;


        GUI.Label(new Rect(500, 200 , 500, 40), rate);
        GUI.Label(new Rect(500, 300 , 500, 40), targetFrameRate);
        GUI.Label(new Rect(500, 400 , 500, 40), frameRate);
        Debug.LogFormat(frameRate);
    }

    bool ShowGUIButton(string buttonName)
    {
        return GUILayout.Button(buttonName, GUILayout.Height(Screen.height / 20), GUILayout.Width(Screen.width / 5));
    }
    void ShowInfoLog(string info)
    {
        infoLog += info;
        infoLog += "\r\n";
        Debug.LogFormat(info);
    }
}
