using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Init : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject obj = Resources.Load<GameObject>("Prefabs/Main");
        Instantiate(obj);
        DestroyImmediate(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
