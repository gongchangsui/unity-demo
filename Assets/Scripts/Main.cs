using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    public Button AudioTest;
    public Button VideoTest;
    public Button BatteryTest;
    public Button NetworkTest;
    public Button SystemInfoTest;
    public Button GyroscopeTest;
    public Button DataSaveTest;
    public Button FileSystemTest;
    public Button VsyncTest;
    public Button SafeAreaTest;
    public Button CrossLanguageTest;
    public Button ColliderTest;
    public Button ColliderTriggerTest;








    
    // Start is called before the first frame update
    void Start()
    {
        AudioTest.onClick.AddListener(AudioTestClick);
        VideoTest.onClick.AddListener(VideoTestClick);
        GyroscopeTest.onClick.AddListener(GyroscopeTestClick);
        BatteryTest.onClick.AddListener(BatteryTestClick);
        NetworkTest.onClick.AddListener(NetworkTestClick);
        SystemInfoTest.onClick.AddListener(SystemInfoTestClick);
        DataSaveTest.onClick.AddListener(DataSaveClick);
        FileSystemTest.onClick.AddListener(FileSystemClick);
        VsyncTest.onClick.AddListener(VsyncTestClick);
        SafeAreaTest.onClick.AddListener(SafeAreaTestClick);
        CrossLanguageTest.onClick.AddListener(CrossLanguageTestClick);
        ColliderTest.onClick.AddListener(ColliderTestClick);
        ColliderTriggerTest.onClick.AddListener(ColliderTriggerTestClick);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void AudioTestClick()
    {
        GameObject obj = Resources.Load<GameObject>("Prefabs/AudioTest");
        Instantiate(obj);
        DestroyImmediate(gameObject);
    }

    private void VideoTestClick()
    {
        GameObject obj = Resources.Load<GameObject>("Prefabs/VideoTest");
        Instantiate(obj);
        DestroyImmediate(gameObject);
    }

    private void GyroscopeTestClick()
    {
        GameObject obj = Resources.Load<GameObject>("Prefabs/GyroscopeTest");
        Instantiate(obj);
        DestroyImmediate(gameObject);
    }

    private void BatteryTestClick()
    {
        GameObject obj = Resources.Load<GameObject>("Prefabs/BatteryTest");
        Instantiate(obj);
        DestroyImmediate(gameObject);
    }

    private void NetworkTestClick()
    {
        GameObject obj = Resources.Load<GameObject>("Prefabs/NetworkTest");
        Instantiate(obj);
        DestroyImmediate(gameObject);
    }

    private void SystemInfoTestClick()
    {
        Jump("Prefabs/SystemInfoTest");
    }

    private void DataSaveClick()
    {
        Jump("Prefabs/DataSaveTest");
    }

    private void FileSystemClick()
    {
        Jump("Prefabs/FileSystemTest");

    }

    private void VsyncTestClick()
    {
        Jump("Prefabs/VsyncTest");

    }
    private void SafeAreaTestClick()
    {
        Jump("Prefabs/SafeAreaTest");

    }
    
    private void CrossLanguageTestClick()
    {
        Jump("Prefabs/CrossLanguageTest");

    }
    
    private void ColliderTestClick()
    {
        Jump("Prefabs/ColliderTest");

    }
    
    private void ColliderTriggerTestClick()
    {
        Jump("Prefabs/ColliderTriggerTest");

    }
    private void Jump(string path)
    {
        GameObject obj = Resources.Load<GameObject>(path);
        Instantiate(obj);
        DestroyImmediate(gameObject);
    }
}
