using UnityEngine;


public class Utils: MonoBehaviour
{
   public static bool ShowGUIButton(string buttonName)
    {
        return GUILayout.Button(buttonName, GUILayout.Height(Screen.height / 20), GUILayout.Width(Screen.width / 5));
    }

    public static void ReturnMain(GameObject gameObject)
    {
        if (ShowGUIButton("返回"))
        {
            GameObject obj = Resources.Load<GameObject>("Prefabs/Main");
            Instantiate(obj);
            DestroyImmediate(gameObject);
        }
    }
}