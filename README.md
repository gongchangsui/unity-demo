# UnityDemo

#### 介绍
验证Unity、Tuanjie 引擎功能

#### 软件架构
1. Scripts -- 脚本, CheckPoins目录中，每个脚本验证一个功能点，并挂在到对应的Prefab
2. Resources/Prefabs -- 每个Prefab验证一个功能点，防止验证点相互影响。
3. Scenes -- 场景， 目前仅有一个。


#### 验证功能
1. AudioTest
   1. 验证语音录制、播放、存储、查看功能。
2. BatteryTest
   1. 验证电池状态
3. CrossLanguageTest
   1. 验证Csharp 调用TS
3. DataSaveTest
   1. 从PlayerPrefs读取、写入数据， 从Bin中读取、写入数据
   2. 从Application.persistanceData 读取、写入数据
4. FileSystemTest
   1. 验证创建目录、创建文件、写入文件、从文件中读数据。
3. GyroscopeTest
   1. 验证实时获取陀螺仪状态
4. NetworkTest
   1. 验证网络请求
5. SafeAreaTest
   1. 安全区测试
5. SystemInfoTest
   1. 验证系统信息
6. VideoTest
   1. 验证视频播放功能。
5. VsyncTest
   1. 验证帧率是否会超过屏幕刷新率以及展示默认帧率

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

